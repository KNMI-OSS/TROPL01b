# TROPL01b



## TROP L01b processor

TROPOMI is an instrument on board of the Sentinel 5 Precursor satellite mission, funded by ESA, and KNMI as principal investigator. 
It measures light from Earth and from the Sun from a Sun-synchronous polar Low Earth Orbit (+/- 800 km). The swath of Earth it images is 
around 2600 km wide, measured in pixels that range from 3 by 3 km directly underneath (at nadir) and larger for larger swath angles. 
It consists of 4 detectors with wavelength ranges between 260 and 2800 nm.

The raw data (level 0) from the instruments is downlinked and stored at a data afacility, where it is transformed into geo-located spectra with proper metadata, the so-called level 1 data product.
This is done by the L01b processor. 

## Docker/Podman

The executable needs to be released as a container. This repository holds the base Docker image in which the processor can be built.

